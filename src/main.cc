#include "renderer.h"

int main()
{
	Window* window = Window::Instance();
	window->Init(1024, 576);

	Camera camera(70.0f, window->AspectRatio(), 0.1f, 100.0f);
	camera.MoveTo(0, 0, 4);

	window->AttachCamera(&camera);

	Renderer r;
	r.AttachWindow(window);
	r.AttachCamera(&camera);

	int id;
	r.BeginMesh(id);
	r.AddMeshTriangle(-0.5f,  0.5f, 0.0f,
	                  -0.5f, -0.5f, 0.0f,
	                   0.5f, -0.5f, 0.0f);
	r.AddMeshTriangle( 0.5f, -0.5f, 0.0f,
	                   0.5f,  0.5f, 0.0f,
	                  -0.5f,  0.5f, 0.0f);	
	r.EndMesh();

	while (1) {
		window->PollEvents();
		r.Render();
	}

	return 0;
}
