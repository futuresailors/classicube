#include "camera.h"

Camera::Camera(float field, float asp, float nv, float fv)
{
	fov = field;
	aspect = asp;
	near = nv;
	far = fv;
}

void Camera::MoveTo(float x, float y, float z)
{
	position = glm::vec3(x, y, z);
}

void Camera::Move(float dx, float dy, float dz)
{
	if (dx && !dy && !dz) {
		double rad = glm::radians(yaw);
		glm::vec3 right(
			glm::sin(rad - glm::half_pi<double>()),
			0,
			glm::cos(rad - glm::half_pi<double>())
		);
		position -= right * dx;
	}

	else if (!dx && !dy && dz) {
		double rad = glm::radians(yaw);
		glm::vec3 direction(glm::sin(rad), 0, glm::cos(rad));
		position += direction * dz;
	}

	else if (!dx && dy && !dz)
		position += glm::vec3(0, dy, 0);
}

void Camera::Rotate(double y, double p)
{
	yaw += y;
	pitch += p;

	if (pitch > 90.0f)
		pitch = 90.0f;
	else if (pitch < -90.0f)
		pitch = -90.0f;

	glm::mat4 mat;
	rotation = glm::rotate(mat, (float)-pitch, glm::vec3(1, 0, 0));
    rotation = glm::rotate(rotation, (float)-yaw, glm::vec3(0, 1, 0));
}

glm::mat4 Camera::GetMatrix()
{
	glm::mat4 mat = glm::perspective(fov, aspect, near, far);
	mat *= rotation;
	mat = glm::translate(mat, -position);
	return mat;
}
