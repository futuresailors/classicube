#include "window.h"

Window* Window::instance = NULL;

Window::~Window()
{
	glfwTerminate();
}

void Window::Init(int w, int h)
{
	if (!glfwInit())
		std::cerr << "Failed to initialize GLFW\n";

	width = w;
	height = h;
	sensitivity = 0.5f;

	handle = glfwCreateWindow(w, h, "ClassiCube", NULL, NULL);

	if (!handle) {
		glfwTerminate();
		std::cerr << "Failed to create window";
	}

	glfwMakeContextCurrent(handle);

	glfwSetKeyCallback(handle, &Window::KeyCallback);
	glfwSetMouseButtonCallback(handle, &Window::MouseButtonCallback);
	glfwSetCursorPosCallback(handle, &Window::MouseMoveCallback);
	glfwSetCursorEnterCallback(handle, &Window::MouseEnterCallback);
	glfwSetScrollCallback(handle, &Window::MouseScrollCallback);
}

void Window::AttachCamera(Camera* cam)
{
	camera = cam;
}

Window* Window::Instance()
{
	if (!instance)
		instance = new Window();

	return instance;
}

GLFWwindow* Window::Handle()
{
	return handle;
}

int Window::Width()
{
	return width;
}

int Window::Height()
{
	return height;
}

float Window::AspectRatio()
{
	return (float)width / (float)height;
}

void Window::SwapBuffers()
{
	glfwSwapBuffers(handle);
}

void Window::PollEvents()
{
	glfwPollEvents();
}

void Window::CaptureMouse()
{
	mouseCaptured = true;
	glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwGetCursorPos(handle, &mouseX, &mouseY);
}

void Window::ReleaseMouse()
{
	mouseCaptured = false;
	glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Window::KeyCallback(GLFWwindow* w, int key, int sc, int act, int mods)
{
	(void)w; // Unused
	Instance()->HandleKey(key, sc, act, mods);
}

void Window::MouseButtonCallback(GLFWwindow* w, int btn, int act, int mods)
{
	(void)w; // Unused
	Instance()->HandleMouseButton(btn, act, mods);
}

void Window::MouseMoveCallback(GLFWwindow* w, double x, double y)
{
	(void)w; // Unused
	Instance()->HandleMouseMove(x, y);
}

void Window::MouseEnterCallback(GLFWwindow* w, int entered)
{
	(void)w; // Unused
	Instance()->HandleMouseEnter(entered);
}

void Window::MouseScrollCallback(GLFWwindow* w, double dx, double dy)
{
	(void)w; // Unused
	Instance()->HandleMouseScroll(dx, dy);
}

void Window::HandleKey(int key, int sc, int act, int mods)
{
	if (key == GLFW_KEY_ESCAPE && act == GLFW_PRESS) {
		ReleaseMouse();
		return;
	}

	// Shitty debug code
	if (key == GLFW_KEY_Q && act != GLFW_RELEASE) {
		camera->Move(0, 0.2, 0);
		return;
	}

	if (key == GLFW_KEY_E && act != GLFW_RELEASE) {
		camera->Move(0, -0.2, 0);
		return;
	}

	if (key == GLFW_KEY_W && act != GLFW_RELEASE) {
		camera->Move(0, 0, -0.2);
		return;
	}

	if (key == GLFW_KEY_S && act != GLFW_RELEASE) {
		camera->Move(0, 0, 0.2);
		return;
	}

	if (key == GLFW_KEY_A && act != GLFW_RELEASE) {
		camera->Move(-0.2, 0, 0);
		return;
	}

	if (key == GLFW_KEY_D && act != GLFW_RELEASE) {
		camera->Move(0.2, 0, 0);
		return;
	}

	(void)sc;
	(void)act;
	(void)mods;
}

void Window::HandleMouseButton(int btn, int act, int mods)
{
	if (!mouseCaptured)
		CaptureMouse();

	(void)btn;
	(void)act;
	(void)mods;
}

void Window::HandleMouseMove(double x, double y)
{
	if (!mouseCaptured)
		return;

	double dx = mouseX - x;
	double dy = mouseY - y;

	mouseX = x;
	mouseY = y;

	camera->Rotate(dx * sensitivity, dy * sensitivity);
}

void Window::HandleMouseEnter(int entered)
{
	// TODO
	(void)entered;
}

void Window::HandleMouseScroll(double dx, double dy)
{
	// TODO
	(void)dx;
	(void)dy;
}
