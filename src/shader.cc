#include "shader.h"

Shader::Shader(const char* file, GLenum shaderType)
{
	type = shaderType;
	shader = glCreateShader(type);

	std::string tmp = ReadFile(file);
	const char* src = tmp.c_str();
	glShaderSource(shader, 1, &src, NULL);
	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		char buf[512];
		glGetShaderInfoLog(shader, 512, NULL, buf);
		std::cerr << "Error compiling " << file << std::endl;
		std::cerr << buf << std::endl;
	}
}

std::string Shader::ReadFile(const char* file)
{
	std::ifstream fs(file, std::ios::in | std::ios::binary);

	if (!fs)
		return "";

	std::stringstream buf;
	buf << fs.rdbuf();

	fs.close();

	return buf.str();
}

void Shader::Delete()
{
	glDeleteShader(shader);
}

Program::Program()
{
	program = glCreateProgram();
}

void Program::AddShader(const char* file, GLenum type)
{
	shaders.emplace_back(file, type);
}

void Program::Attach()
{
	std::vector<Shader>::iterator i;
	for (i = shaders.begin(); i != shaders.end(); i++)
		glAttachShader(program, i->shader);
}

void Program::Bind(std::vector<std::string> &outputs)
{
	for (unsigned i = 0; i < outputs.size(); i++)
		glBindFragDataLocation(program, i, outputs[i].c_str());
}

void Program::Link()
{
	glLinkProgram(program);

	GLint status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (status == GL_FALSE) {
		char buf[512];
		glGetProgramInfoLog(program, 512, NULL, buf);
		std::cerr << buf << std::endl;
	}
}

void Program::Use()
{
	glUseProgram(program);
}

GLint Program::GetAttribLocation(const char* attrib)
{
	return glGetAttribLocation(program, attrib);
}

GLint Program::GetUniformLocation(const char* uniform)
{
	return glGetUniformLocation(program, uniform);
}

void Program::EnableAttrib(const char* attrib, GLint size, GLenum type, GLboolean norm,
	                       GLsizei stride, const GLvoid* offs)
{
	GLuint loc = GetAttribLocation(attrib);
	glEnableVertexAttribArray(loc);
	glVertexAttribPointer(loc, size, type, norm, stride, offs);
}

void Program::AttachTexture(const char* uniform, Texture& texture)
{
	glUniform1i(GetUniformLocation(uniform), texture.Number());
}

void Program::Delete()
{
	std::vector<Shader>::iterator i;
	for (i = shaders.begin(); i != shaders.end(); i++)
		i->Delete();

	glDeleteProgram(program);
}
