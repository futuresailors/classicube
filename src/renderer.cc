#include "renderer.h"

Renderer::Renderer()
{
	glewExperimental = GL_TRUE;
	glewInit();

	program = new Program();
	program->AddShader("res/cube.vs", GL_VERTEX_SHADER);
	program->AddShader("res/cube.fs", GL_FRAGMENT_SHADER);
	program->Attach();
	std::vector<std::string> binds { "color" };
	program->Bind(binds);
	program->Link();
	program->Use();
}

Renderer::~Renderer()
{
	std::vector<Mesh>::iterator i;
	for (i = meshes.begin(); i != meshes.end(); i++)
		glDeleteBuffers(1, &(i->vbo));

	program->Delete();
	delete program;
}

void Renderer::AttachWindow(Window* win)
{
	window = win;
}

void Renderer::AttachCamera(Camera* cam)
{
	camera = cam;
}

void Renderer::BeginMesh(int& id)
{
	id = meshes.size();
	meshes.emplace_back();
	currentMesh = &meshes.back();

	glGenVertexArrays(1, &(currentMesh->vao));
	glBindVertexArray(currentMesh->vao);

	glGenBuffers(1, &(currentMesh->vbo));
}

void Renderer::AddMeshTriangle(float vx1, float vy1, float vz1,
                               float vx2, float vy2, float vz2,
                               float vx3, float vy3, float vz3)
{
	currentMesh->vertices.push_back(vx1);
	currentMesh->vertices.push_back(vy1);
	currentMesh->vertices.push_back(vz1);

	currentMesh->vertices.push_back(vx2);
	currentMesh->vertices.push_back(vy2);
	currentMesh->vertices.push_back(vz2);

	currentMesh->vertices.push_back(vx3);
	currentMesh->vertices.push_back(vy3);
	currentMesh->vertices.push_back(vz3);
}

void Renderer::EndMesh()
{
	glBindBuffer(GL_ARRAY_BUFFER, currentMesh->vbo);
	glBufferData(
		GL_ARRAY_BUFFER,
		currentMesh->vertices.size() * sizeof(float),
		currentMesh->vertices.data(),
		GL_DYNAMIC_DRAW
	);
	program->EnableAttrib("pos", 3, GL_FLOAT, GL_FALSE, 0, 0);

	currentMesh = NULL;
}

void Renderer::Render()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	std::vector<Mesh>::iterator i;
	for (i = meshes.begin(); i != meshes.end(); i++) {
		GLint loc = program->GetUniformLocation("camera");
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(camera->GetMatrix()));

		glBindVertexArray(i->vao);
		glBindBuffer(GL_ARRAY_BUFFER, i->vbo);
		glDrawArrays(GL_TRIANGLES, 0, i->vertices.size() / 3);
	}

	window->SwapBuffers();
}
