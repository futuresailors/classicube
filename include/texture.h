#pragma once

#include <GL/glew.h>
#include <SOIL/SOIL.h>

class Texture {
private:
	GLuint texture;

	int number;

	int width;
	int height;

public:
	Texture(const char* file, int num);

	int Number();

	void Delete();
};
