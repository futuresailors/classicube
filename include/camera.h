#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <iostream>

class Camera {
private:
	float fov;
	float aspect;
	float near;
	float far;

	double pitch;
	double yaw;

	glm::mat4 rotation;
	glm::vec3 position;

public:
	Camera(float field, float asp, float nv, float fv);

	void MoveTo(float x, float y, float z);
	void Move(float dx, float dy, float dz);
	void Rotate(double y, double p);

	glm::mat4 GetMatrix();
};
