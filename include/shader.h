#pragma once

#include <GL/glew.h>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include "texture.h"

class Shader {
private:
	friend class Program;
	GLenum type;
	GLuint shader;

	std::string ReadFile(const char* file);

public:
	Shader(const char* file, GLenum shaderType);

	void Delete();
};

class Program {
private:
	GLuint program;
	std::vector<Shader> shaders;

public:
	Program();

	void AddShader(const char* file, GLenum type);
	void Attach();
	void Bind(std::vector<std::string> &outputs);
	void Link();
	void Use();

	GLint GetAttribLocation(const char* attrib);
	GLint GetUniformLocation(const char* uniform);
	void EnableAttrib(const char* attrib, GLint size,
	                  GLenum type, GLboolean normalize,
	                  GLsizei stride, const GLvoid* offset);

	void AttachTexture(const char* uniform, Texture& texture);

	void Delete();
};
