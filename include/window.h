#pragma once

#include <GLFW/glfw3.h>
#include <iostream>
#include "camera.h"

class Window {
private:
	static Window* instance;

	GLFWwindow* handle;
	Camera* camera;

	int width;
	int height;

	double mouseX;
	double mouseY;

	float sensitivity;

	bool mouseCaptured;

	Window() {}
	Window(Window const&) {}
	void operator=(Window const&) {}

public:
	~Window();

	static Window* Instance();

	void Init(int w, int h);
	void AttachCamera(Camera* cam);

	GLFWwindow* Handle();

	int Width();
	int Height();
	float AspectRatio();

	static void KeyCallback(GLFWwindow* w, int key, int sc,int act, int mods);
	static void MouseButtonCallback(GLFWwindow* w, int btn, int act, int mods);
	static void MouseMoveCallback(GLFWwindow* w, double x, double y);
	static void MouseEnterCallback(GLFWwindow* w, int entered);
	static void MouseScrollCallback(GLFWwindow* w, double dx, double dy);

	void HandleKey(int key, int sc, int act, int mods);
	void HandleMouseButton(int btn, int act, int mods);
	void HandleMouseMove(double x, double y);
	void HandleMouseEnter(int entered);
	void HandleMouseScroll(double dx, double dy);

	void CaptureMouse();
	void ReleaseMouse();

	void SwapBuffers();
	void PollEvents();
};
