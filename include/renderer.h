#pragma once

#include <GL/glew.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include "window.h"
#include "camera.h"
#include "shader.h"

struct Mesh {
	std::vector<float> vertices;
	GLuint vao;
	GLuint vbo;
};

class Renderer {
private:
	Window* window;
	Camera* camera;

	Program* program;

	std::vector<Mesh> meshes;
	Mesh* currentMesh;

public:
	Renderer();
	~Renderer();

	void AttachWindow(Window* win);
	void AttachCamera(Camera* cam);

	void BeginMesh(int& id);
	void AddMeshTriangle(float vx1, float vy1, float vz1,
		                 float vx2, float vy2, float vz2,
		                 float vx3, float vy3, float vz3);
	void EndMesh();

	void Render();
};
