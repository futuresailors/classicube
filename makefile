CC      := g++
CFLAGS  := -std=c++11 -W -Wall -Werror -Wextra -O3 -Iinclude
LFLAGS  :=
SOURCES := $(shell find ./src -name '*.cc')
OBJECTS := $(SOURCES:.cc=.o)
LIBS    := -lGLEW -lGL -lglfw -lSOIL

all: $(SOURCES) classicube

classicube: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LIBS)
	mkdir -p build
	mv src/*.o build

.cc.o:
	$(CC) $(CFLAGS) $< -c -o $@ $(LIBS)

clean:
	rm -rf build src/*.o classicube
